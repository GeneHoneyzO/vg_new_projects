import 'package:flutter/material.dart';
import 'package:percent_helper/percent_helper.dart';
import 'dart:ui';

export 'package:percent_helper/percent_helper.dart';

class AdModel {
  const AdModel({this.title, this.imgURL, this.content});

  final String title;
  final String imgURL;
  final String content;
}

class QuizModel {
  String title;
  String type;
  HelpModel help;
  String image;
  String content;
  List<String> options;
  int right;
  QuizModel(this.title, this.image, this.content, this.type, this.help);
  QuizModel.map(Map map) {
    title = map['title'];
    image = map['image'];
    content = map['content'];
    options = map['options'];
    right = map['right'];
    type = map['type'];
    help = HelpModel.map(map['help']);
  }
}

class HelpModel {
  String title;
  String image;
  String content;
  HelpModel(this.title, this.image, this.content);
  HelpModel.map(Map map) {
    title = map['title'];
    image = map['image'];
    content = map['content'];
  }
}

class ImageContentWidget extends StatelessWidget {
  const ImageContentWidget({Key key, @required this.imgURL, this.texts})
      : super(key: key);

  final String imgURL;
  final List<String> texts;

  @override
  Widget build(BuildContext context) {
    return Stack(
      alignment: Alignment.bottomLeft,
      children: <Widget>[
        Image.network(
          imgURL,
          fit: BoxFit.fill,
          width: width(375),
        ),
        Column(
          mainAxisAlignment: MainAxisAlignment.end,
          children: texts.map((text) {
            return Container(
              color: Colors.black26,
              height: 20.0,
              child: Text(
                text,
                style: TextStyle(color: Colors.white, fontSize: width(13)),
              ),
              alignment: Alignment.centerLeft,
              padding: EdgeInsets.only(left: width(20.0)),
              width: width(375),
            );
          }).toList(),
        )
      ],
    );
  }
}

Widget cardWidget(Widget child,
    {double radius: 12.0, Color backColor: const Color(0xffffffff)}) {
  return new DecoratedBox(
    decoration: new ShapeDecoration(
      shadows: [
        new BoxShadow(
            color: new Color(0x40003E73),
            offset: new Offset(0.0, width(2)),
            blurRadius: width(2))
      ],
      shape: new RoundedRectangleBorder(
        borderRadius: new BorderRadius.circular(radius),
      ),
    ),
    child: new ClipRRect(
      borderRadius: new BorderRadius.circular(radius),
      child: DecoratedBox(
        decoration: new BoxDecoration(color: backColor),
        child: child,
      ),
    ),
  );
}

class ClubModel {
  String name;
  String logo;
  String gameType;
  ClubModel(this.name, this.logo, {this.gameType: 'dota'});
  ClubModel.map(Map map) {
    name = map['name'];
    logo = map['image'];
    gameType = "dota";
  }
}
