import 'package:flutter/material.dart';

import '../common_widget.dart';

class ExamPage extends StatefulWidget {
  @override
  _ExamPageState createState() => new _ExamPageState();
}

class _ExamPageState extends State<ExamPage> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: new AppBar(
        title: new Text('终极决选'),
      ),
      backgroundColor: new Color(0xffF0EAEB),
      body: Center(
        child: body(),
      ),
    );
  }
}

body() {
  return new CustomScrollView(
    slivers: <Widget>[
      new SliverList(
        delegate: new SliverChildBuilderDelegate(
          (BuildContext context, int index) {
            return table(index);
          },
          childCount: 3,
        ),
      ),
    ],
    shrinkWrap: true,
    physics: NeverScrollableScrollPhysics(),
  );
}

table(index) {
  return Padding(
    padding: new EdgeInsets.only(
      left: width(31),
      right: width(31),
      bottom: width(40),
    ),
    child: new SizedBox(
      child: cardWidget(new Text('$index'), radius: width(15)),
      height: width(120),
    ),
  );
}

date(day) {
  return new SliverList(
      delegate: new SliverChildBuilderDelegate((c, i) {
    return Container(
      alignment: Alignment.center,
      height: height(20),
      child: new Text(
        '${DateTime.now().subtract(new Duration(days: day)).toIso8601String().substring(0, 10)}',
        textAlign: TextAlign.center,
      ),
    );
  }, childCount: 1));
}
