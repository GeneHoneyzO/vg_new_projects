import 'package:flutter/material.dart';
import 'home_page_top.dart';
import 'home_page_mid.dart';

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => new _HomePageState();
}

class _HomePageState extends State<HomePage> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: new AppBar(
        title: new Text('我的家园'),
        actions: <Widget>[
          IconButton(
              icon: Icon(Icons.search),
              onPressed: () {
                Navigator.of(context).pushNamed('search');
              })
        ],
        leading: IconButton(
            icon: Icon(Icons.more_horiz),
            onPressed: () {
              print('hello');
            }),
      ),
      body: new Center(
        child: new Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
//            new Divider(
//              height: height(13),
//              color: Colors.transparent,
//            ),
            new HomePageTop(),
            new Mid()
          ],
        ),
      ),
    );
  }
}
