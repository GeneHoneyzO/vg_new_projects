import 'package:flutter/material.dart';
import '../common_widget.dart';
import 'home_model.dart';

class Mid extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return new MidState();
  }
}

class MidState extends State<Mid> {
  List<AdModel> adModels = [];
  @override
  void initState() {
    super.initState();
    var ads = homeModel['news'];
    var adModel = ads.map((dict) {
      return AdModel(
          imgURL: dict['image'],
          title: dict['title'],
          content: dict['content']);
      ;
    }).toList();
    setState(() {
      adModels = adModel;
    });
  }

  @override
  Widget build(BuildContext context) {
    return table();
  }

  table() {
    return new Expanded(
      child: new ListView.builder(
        itemBuilder: (ctx, index) {
          var model = adModels[index];
          return Padding(
            padding: new EdgeInsets.only(
                left: width(31), right: width(31), bottom: height(14)),
            child: new SizedBox(
              child: cardWidget(
                  ImageContentWidget(
                    imgURL: adModels[index].imgURL,
                    texts: [model.content, model.title],
                  ),
                  radius: width(4)),
              height: width(64),
            ),
          );
        },
        itemCount: adModels.length,
        padding: new EdgeInsets.only(top: height(14)),
      ),
    );
  }
}
