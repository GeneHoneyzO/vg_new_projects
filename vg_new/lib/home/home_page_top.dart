import 'package:flutter/material.dart';
import '../common_widget.dart';
import 'home_model.dart';

class HomePageTop extends StatefulWidget {
  @override
  _HomePageTopState createState() => new _HomePageTopState();
}

class _HomePageTopState extends State<HomePageTop>
    with SingleTickerProviderStateMixin {
  TabController _tabController;

  @override
  void initState() {
    super.initState();
    var ads = homeModel['ads'];
    var adModel = ads.map((dict) {
      return AdModel(
          imgURL: dict['image'],
          title: dict['title'],
          content: dict['content']);
      ;
    }).toList();
    setState(() {
      adModels = adModel;
    });
    _tabController = TabController(vsync: this, length: adModels.length);
  }

  @override
  void dispose() {
    _tabController.dispose();
    super.dispose();
  }

  void _nextPage(int delta) {
    final int newIndex = _tabController.index + delta;
    if (newIndex < 0 || newIndex >= _tabController.length) return;
    _tabController.animateTo(newIndex);
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
//        TextField(
//          decoration: InputDecoration(
//              labelText: '今天看什么', contentPadding: EdgeInsets.all(12.0)),
//
//        ),
        new Container(
          height: 10.0,
        ),
        new Container(
          height: width(141),
          width: width(375),
          child: new TabBarView(
            controller: _tabController,
            children: adModels.map((AdModel choice) {
              return ChoiceCard(choice: choice);
            }).toList(),
          ),
        ),
      ],
    );
  }
}

List<AdModel> adModels = [];

class ChoiceCard extends StatelessWidget {
  const ChoiceCard({Key key, this.choice}) : super(key: key);

  final AdModel choice;

  @override
  Widget build(BuildContext context) {
    return new Card(
      clipBehavior: Clip.hardEdge,
      child: new ImageContentWidget(
        imgURL: choice.imgURL,
        texts: [choice.title],
      ),
      shape: new RoundedRectangleBorder(
          borderRadius: new BorderRadius.circular(width(20))),
    );
  }
}
