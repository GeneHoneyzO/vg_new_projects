import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'home/home_page.dart';
import 'train/train_page.dart';
import 'match/match_page.dart';
import 'exam/exam_page.dart';
import 'search_page.dart';
import 'package:percent_helper/percent_helper.dart' as percent;

void main() {
  percent.setSource(667, 375, safeArea: {
    "left": true,
    "right": true,
    "top": true,
    "bottom": true,
  });
  runApp(new MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return new MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(primaryColor: Colors.red),
      home: new MyHomePage(title: 'Flutter Demo Home Page'),
      initialRoute: '/',
      routes: {
        'search': (ctx) {
          return SearchPage();
        }
      },
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _MyHomePageState createState() => new _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  @override
  Widget build(BuildContext context) {
    return new CupertinoTabScaffold(
        tabBar: new CupertinoTabBar(
            activeColor: Colors.red,
            inactiveColor: Colors.blueGrey,
            items: [
              new BottomNavigationBarItem(
                  icon: ImageIcon(
                    AssetImage('images/news.png'),
                  ),
                  title: Text('首页')),
              new BottomNavigationBarItem(
                  icon: ImageIcon(AssetImage('images/schedule.png')),
                  title: Text('赛程')),
              new BottomNavigationBarItem(
                  icon: ImageIcon(AssetImage('images/book.png')),
                  title: Text('教学')),
              new BottomNavigationBarItem(
                  icon: ImageIcon(AssetImage('images/exam.png')),
                  title: Text('考试')),
            ]),
        tabBuilder: (ctx, index) {
          switch (index) {
            case 0:
              return HomePage();
            case 1:
              return MatchPage();
            case 2:
              return TrainPage();
            case 3:
              return ExamPage();
            default:
              return Scaffold(
                body: new Center(
                  child: Text('HEHE'),
                ),
              );
              break;
          }
        });
  }
}
