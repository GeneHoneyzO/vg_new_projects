import 'package:flutter/material.dart';

import '../common_widget.dart';
import 'match_model.dart';

class MatchPage extends StatefulWidget {
  @override
  _MatchPageState createState() => new _MatchPageState();
}

class MatchModel {
  String timeText;
  ClubModel clubFirst;
  ClubModel clubSecond;
  MatchModel(this.timeText, this.clubFirst, this.clubSecond);
  MatchModel.map(Map map) {
    timeText = map['timeText'];
    clubFirst = ClubModel.map(map['first']);
    clubSecond = ClubModel.map(map['second']);
  }
}

class _MatchPageState extends State<MatchPage> {
  List<List<MatchModel>> matchesModel = [];

  @override
  void initState() {
    super.initState();
    List<List<Map>> matchesJsonList = matchMap['matches'];
    var matchesModelTemp = matchesJsonList.map((list) {
      return list.map((model) {
        return MatchModel.map(model);
      }).toList();
    }).toList();
    matchesModel = matchesModelTemp;
  }

  List<List<MatchModel>> matchGroup = [];

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: new AppBar(
        title: new Text('我的家园'),
      ),
      backgroundColor: new Color(0xffF0EAEB),
      body: new Stack(
        alignment: AlignmentDirectional.topCenter,
        children: [
          DecoratedBox(
            decoration: new BoxDecoration(color: Colors.white),
            child: new Container(
              alignment: Alignment.center,
              constraints: new BoxConstraints.expand(height: height(28)),
              child: new Text(
                '即将到来的比赛',
                textAlign: TextAlign.center,
              ),
            ),
          ),
          table()
        ],
      ),
    );
  }

  matchCell(index, MatchModel model, {bool isCurrent = false}) {
    return Padding(
      padding: new EdgeInsets.only(
        left: width(31),
        right: width(31),
        bottom: width(14),
      ),
      child: new Container(
        child: cardWidget(
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                Column(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: <Widget>[
                    Image.network(
                      model.clubFirst.logo,
                      width: width(50),
                      height: width(50),
                      fit: BoxFit.cover,
                    ),
                    Text(model.clubFirst.name)
                  ],
                ),
                Padding(
                  padding: const EdgeInsets.only(top: 12),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Image.asset(
                        "images/match_versus.png",
                        width: width(25),
                        height: width(25),
                        fit: BoxFit.cover,
                      ),
                      Padding(
                        padding: const EdgeInsets.only(top: 8),
                        child: Text(
                          "还有10分钟",
                          style:
                              TextStyle(color: Color(0xff989898), fontSize: 12),
                        ),
                      )
                    ],
                  ),
                ),
                Column(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: <Widget>[
                    Image.network(
                      model.clubSecond.logo,
                      width: width(50),
                      height: width(50),
                      fit: BoxFit.cover,
                    ),
                    Text(model.clubSecond.name)
                  ],
                ),
              ],
            ),
            backColor: isCurrent ? Color(0xffFADDCB) : Colors.white,
            radius: width(8)),
        height: width(88),
      ),
    );
  }

  table() {
    List<Widget> normalCells = [
      SliverPadding(padding: EdgeInsets.all(6.0)),
      new SliverList(
        delegate: new SliverChildBuilderDelegate(
          (BuildContext context, int index) {
            return matchCell(0, MatchModel.map(matchMap['comming']),
                isCurrent: true);
          },
          childCount: 1,
        ),
      ),
    ];

    for (int i = 0; i < matchesModel.length; i++) {
      var list = matchesModel[i];
      normalCells.add(date(i));
      normalCells.add(SliverList(
        delegate: new SliverChildBuilderDelegate(
          (BuildContext context, int index) {
            return matchCell(index, list[index]);
          },
          childCount: list.length,
        ),
      ));
    }

    normalCells.add(SliverPadding(padding: EdgeInsets.all(40.0)));

    return new Padding(
      padding: new EdgeInsets.only(top: height(30)),
      child: new CustomScrollView(slivers: normalCells),
    );
  }
}

date(day) {
  return new SliverList(
      delegate: new SliverChildBuilderDelegate((c, i) {
    return Padding(
      padding: const EdgeInsets.only(bottom: 8.0),
      child: Container(
        alignment: Alignment.center,
        height: height(25),
        child: new Text(
          '${DateTime.now().subtract(new Duration(days: day)).toIso8601String().substring(0, 10)}',
          textAlign: TextAlign.center,
          style: TextStyle(fontSize: 19, color: Colors.black54),
        ),
      ),
    );
  }, childCount: 1));
}
