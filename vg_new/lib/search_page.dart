import 'package:flutter/material.dart';

class SearchPage extends StatefulWidget {
  @override
  _SearchPageState createState() => new _SearchPageState();
}

class _SearchPageState extends State<SearchPage> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: new AppBar(
        title: new Text('搜搜看看'),
      ),
      body: new Container(
        child: TextField(
          onSubmitted: (text) {
            Navigator.of(context).pop();
          },
        ),
      ),
    );
  }
}
