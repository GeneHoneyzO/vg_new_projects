var trainMmap = {
  "game": "dota",
  "quizs": [
    {
      "title": "Dota2英雄考试",
      "options": ["死亡之后", "拿取肉山盾之后"],
      "right": 0,
      "type": "choice",
      "image":
          "https://static.cnbetacdn.com/thumb/article/2018/1217/21cc612791ef315.jpg",
      "help": {
        "title": "Dota2小介绍",
        "content":
            "Roshan，通常是指魔兽RPG地图DOTA和多人在线战术竞技游戏DOTA2中的大boss，国人一般称作肉山，即Roshan的音译，本来Roshan体积就庞大如山，又非常耐打故称肉山。",
        "image":
            "https://static.cnbetacdn.com/thumb/article/2018/1217/21cc612791ef315.jpg"
      },
      "content": "拿取肉山盾的英雄5分钟内未死亡，肉山盾自动消失。请问时间是从肉山死亡之后开始计算还是拿取肉山盾后时间计算？"
    },
    {
      "title": "Dota2英雄考试",
      "options": ["死亡之后", "拿取肉山盾之后"],
      "right": 0,
      "type": "choice",
      "help": {
        "title": "Dota2小介绍",
        "content":
            "Roshan，通常是指魔兽RPG地图DOTA和多人在线战术竞技游戏DOTA2中的大boss，国人一般称作肉山，即Roshan的音译，本来Roshan体积就庞大如山，又非常耐打故称肉山。",
        "image":
            "https://static.cnbetacdn.com/thumb/article/2018/1217/21cc612791ef315.jpg"
      },
      "content": "拿取肉山盾的英雄5分钟内未死亡，肉山盾自动消失。请问时间是从肉山死亡之后开始计算还是拿取肉山盾后时间计算？"
    },
  ],
};
