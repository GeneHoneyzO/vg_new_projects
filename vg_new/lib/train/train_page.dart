import 'package:flutter/material.dart';
import 'package:flip_card/flip_card.dart';
import '../common_widget.dart';
import 'train_model.dart';
import 'package:flutter/cupertino.dart';

class TrainPage extends StatefulWidget {
  @override
  _TrainPageState createState() => new _TrainPageState();
}

class _TrainPageState extends State<TrainPage> {
  int _currentQuizIndex = 0;

  QuizModel get _currentQuiz {
    if (_currentQuizIndex < quizsModel.length) {
      return quizsModel[_currentQuizIndex];
    }
    return QuizModel("null", "", "", "", HelpModel("Help", "", "cc"));
  }

  List<QuizModel> quizsModel = [];

  @override
  void initState() {
    super.initState();
    List<Map> matchesJsonList = trainMmap['quizs'];
    var matchesModelTemp = matchesJsonList.map((dict) {
      return QuizModel.map(dict);
    }).toList();
    quizsModel = matchesModelTemp;
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: new AppBar(
        title: new Text('牛刀小试'),
      ),
      backgroundColor: new Color(0xffF0EAEB),
      body: new Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          new Divider(
            height: height(23),
            color: Colors.transparent,
          ),
          bannerPart(),
          new Divider(
            height: height(14),
            color: Colors.transparent,
          ),
          cardsBody()
        ],
      ),
    );
  }

  bannerPart() {
    return new SizedBox(
      width: width(320),
      child: cardWidget(
          new Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              new Stack(
                children: <Widget>[
                  new Image.asset(
                    'images/dota_banner.png',
                    fit: BoxFit.fill,
                    height: width(62),
                    width: width(258),
                  ),
                  new Column(
                    children: <Widget>[
                      new Image.asset('images/dota_icon.png'),
                      new Text('dota2')
                    ],
                  )
                ],
              ),
              GestureDetector(
                child: new Container(
                  width: width(62),
                  height: width(62),
                  decoration: new BoxDecoration(color: Colors.white),
                  child: new Image.asset('images/train_down_arrow.png'),
                ),
                onTap: () {
                  showModalBottomSheet(
                      context: context,
                      builder: (c) {
                        return new CupertinoPicker.builder(
                            childCount: 2,
                            itemExtent: 40.0,
                            onSelectedItemChanged: (i) {
                              print(i);
                              Navigator.of(context).pop();
                            },
                            itemBuilder: (c, i) {
                              return VGText("$i");
                            });
                      });
                },
              )
            ],
          ),
          radius: 6.0),
    );
  }

  cardsBody() {
    return new SizedBox(
        height: height(432),
        width: width(320),
        child: new FlipCard(front: frontCard(), back: backCard()));
  }

  frontCard() {
    List<Widget> contents = [
      new VGText(_currentQuiz.title),
      Padding(
        padding: const EdgeInsets.only(bottom: 8.0),
        child: new VGText(_currentQuiz.content),
      ),
    ];
    var optWidget = Column(
      children: _currentQuiz.options.map((opt) {
        return Padding(
          padding: const EdgeInsets.only(bottom: 8.0),
          child: cardWidget(
              Container(
                alignment: Alignment.centerLeft,
                child: VGText(opt),
                height: height(32),
                padding: EdgeInsets.only(left: 8.0),
              ),
              radius: 5.0,
              backColor: Color(0xffFF8383)),
        );
      }).toList(),
    );

    contents.add(optWidget);
    contents.add(Padding(
      padding: const EdgeInsets.all(8.0),
      child: new Image.asset(
        'images/train_help.png',
        fit: BoxFit.cover,
      ),
    ));

    return cardWidget(new Stack(
      children: <Widget>[
        new Image.network(_currentQuiz.image),
        dartShadow(),
        Padding(
          padding: EdgeInsets.all(width(12)),
          child: new Column(
            children: contents,
          ),
        )
      ],
    ));
  }

  backCard() {
    return cardWidget(
      new Stack(
        children: <Widget>[
          new Image.network(_currentQuiz.help.image),
          dartShadow(),
          Padding(
            padding: const EdgeInsets.all(12.0),
            child: new Column(
              children: <Widget>[
                Padding(
                  padding: const EdgeInsets.only(bottom: 8.0),
                  child: new VGText(
                    _currentQuiz.help.title,
                    fontSize: 17.0,
                  ),
                ),
                new VGText(_currentQuiz.help.content),
              ],
            ),
          )
        ],
      ),
    );
  }
}

dartShadow() {
  return DecoratedBox(
    decoration: new ShapeDecoration(
      shape: RoundedRectangleBorder(),
      shadows: [
        new BoxShadow(
            color: new Color(0x44000000),
            spreadRadius: width(60.0),
            offset: new Offset(0.0, width(-60)),
            blurRadius: width(60))
      ],
    ),
    child: Container(
      height: 200.0,
      color: Colors.transparent,
    ),
  );
}

class VGText extends StatelessWidget {
  final String text;
  final double fontSize;

  VGText(this.text, {this.fontSize: 15.0});
  @override
  Widget build(BuildContext context) {
    return Text(
      text,
      style: TextStyle(color: Colors.white, fontSize: fontSize),
    );
  }
}
