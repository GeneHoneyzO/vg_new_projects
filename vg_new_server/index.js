var path = require("path");
var express = require("express");
var config = require("config-lite")(__dirname);
var routes = require("./routes");

var bodyParser = require("body-parser");

var app = express();
var allowCrossDomain = function(req, res, next) {
  res.header('Access-Control-Allow-Origin', '*');
  res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE');
  res.header('Access-Control-Allow-Headers', 'Content-Type');
  next();
};
app.use(allowCrossDomain);
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

// 设置静态文件目录
app.use(express.static(path.join(__dirname, "views")));
app.use(express.static(path.join(__dirname, "public")));
// 路由
routes(app);
// 监听端口，启动程序
app.listen(config.port, function() {
  console.log(`listening on port ${config.port}`);
});
