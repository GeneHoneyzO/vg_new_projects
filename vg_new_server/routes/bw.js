var express = require("express");
var router = express.Router();

router.get("/website/:platform", function (req, res, next) {
    var url;

    if (req.params.platform != null) {
        if (req.params.platform == "ios") {
            url = "http://www.ksben.com/";
        } else {
            url = "https://www.bwin.com";
        }
    } else {
        url = "https://www.bwin.com";
    }
    res.send({
        'url': url
        // 'url': 'http://www.ksben.com/'
    });
})
module.exports = router;