var express = require('express')
var router = express.Router()
var checkLogin = require('../middlewares/check').checkLogin
var uploadFile = require('../utils/FileUpload')

// POST 上传
router.post('/', function (req, res, next) {
  uploadFile(req, res, next)
});
module.exports = router;