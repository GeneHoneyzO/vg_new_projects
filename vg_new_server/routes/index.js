module.exports = function (app) {
    app.use('/images', require('./images'));
    app.use('/', require('./user'));
    app.use('/bw', require('./bw'));
};
