var fs = require("fs");
var path = require("path");
var sha1 = require("sha1");
var express = require("express");
var router = express.Router();

var UserModel = require("../models/users");
var checkNotLogin = require("../middlewares/check").checkNotLogin;

// POST /signup 用户注册
router.post("/register", checkNotLogin, function(req, res, next) {
  var password = req.body.password;
  var ip =
    req.headers["x-forwarded-for"] ||
    req.ip ||
    req.connection.remoteAddress ||
    req.socket.remoteAddress ||
    req.connection.socket.remoteAddress ||
    "";
  // 明文密码加密
  password = sha1(password);
  const name = NameModel.getUnusedNickName(ip).then(nameModel => {
    // 待写入数据库的用户信息
    var user = {
      ip: ip,
      name: nameModel.nickName,
      nickName: nameModel.nickName,
      password: password,
      expression: 'We are beautiful',
      avatar: 'http://www.pictures88.com/p/hi_hello/hi_hello_003.jpg',
    };
    // 用户信息写入数据库
    //todo
  });
});

// POST /signup 用户登录
router.post("/login", function (req, res, next) {
  var password = req.body.password;
  var nickName = req.body.nickName;
  console.log('用户:', nickName, '尝试登陆')
  // 明文密码加密
  password = sha1(password);
  //todo
});

// POST 苹果API 测试用
router.get("/logout", function(req, res, next) {
  console.log('logout');
  UserModel.test();
  res.send({
    code: 0,
    data: {
      version: '0.1'
    }
  });
  next();
});

module.exports = router;
