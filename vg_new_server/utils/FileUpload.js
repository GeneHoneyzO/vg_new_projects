var shell = require("shelljs");
var ffmpeg = require("fluent-ffmpeg");
const querystring = require("querystring");
const Buffer = require("buffer").Buffer;
const fs = require("fs");
const path = require("path");
function upload(req, res, next) {
  req.setEncoding("binary");
  const ip = req.headers['Host']
  var body = ""; // 文件数据
  var fileName = ""; // 文件名
  // 边界字符串
  var boundary = req.headers["content-type"]
    .split("; ")[1]
    .replace("boundary=", "");
  req.on("data", function(chunk) {
    body += chunk;
  });

  req.on("end", function() {
    file = querystring.parse(body, "\r\n", ":");
    fileInfoArray = getFileInfo(file);
    var fileNameArray = [];
    for (const key in fileInfoArray) {
      fileNameArray.push(getFileName(fileInfoArray[key]));
    }
    binaryData = getOriginFileDataArray(body, boundary);
    var imagesSrc = [];
    var videoURLs = []
    var videoCapURL = ''
    for (key in binaryData) {
      var videoURL = ''
      // 保存文件
      var fileName =
        key < fileNameArray.length ? fileNameArray[key] : "nullName";
      
      console.log('fileName: ', fileName)
      var pathOfFile = '';
      const components = fileName.split(".")
      var subfix = components[components.length - 1];
      if (subfix == "mp4") {
        pathOfFile = path.join("public/boonsResource/video", fileName)
        var prefix = fileName.split(".")[0];
        videoURL = path.join('boonsResource/video', fileName)
        var root = path.join(__dirname, "../");
        var vFile = path.join(root, "public", videoURL);
        fileName = prefix + '_' + Date.parse(new Date()).toString() + '.jpg'
        videoCapURL = path.join('boonsResource/video/videoCap', fileName)
        const targetOfImageFile = path.join(root, "public", videoCapURL);  
        fs.writeFileSync(pathOfFile, binaryData[key], "binary");
        videoURLs.push(videoURL)
        // const ffmpegPathInCentos = "/tttt/ffmpeg-3.1/ffmpeg"
        const ffmpegPathInCentos = "ffmpeg"
        var cmd = ffmpegPathInCentos + " -i " + vFile + " -ss 00:00:01 -f image2 " + targetOfImageFile;
        console.log('cmd: ', cmd);
        shell.exec(cmd);
        shell.exec('y')
      } else if(subfix == "jpg" || subfix == "png" || subfix == "jpeg" || subfix == "gif") {
        const imageDir = 'public/boonsResource/image'
        pathOfFile = path.join(imageDir, fileName)
        if (!fs.existsSync(imageDir)) {
          fs.mkdirSync(imageDir)
        }
        imagesSrc.push(path.join("boonsResource/image", fileName));
        fs.writeFileSync(pathOfFile, binaryData[key], "binary");
      }
    }
    console.log('videoURLs ', videoURLs)
    console.log('videoCapURL ', videoCapURL)
    res.send({
      code: 0,
      images: imagesSrc,
      videos: videoURLs,
      videoCap: videoCapURL
    });
    next();
  });
}

function getContentType(file) {
  type = file["Content-Type"];
  if (typeof type == "string") {
    return type.substring(1);
  } else if (type instanceof Array) {
    return type[0].substring(1);
  } else {
    return "none";
  }
}

function getFileInfo(file) {
  info = file["Content-Disposition"];
  if (typeof info == "string") {
    return [info];
  } else if (info instanceof Array) {
    return info;
  } else {
    return ["none"];
  }
}

function getFileName(fileInfo) {
  pattern = /filename="[\s\S]*"/;
  result = pattern.exec(fileInfo);
  if (result == null) {
    return;
  }
  fileString = result[0];
  fileName = /"[\s\S]*?"/.exec(fileString)[0];
  fileName = fileName.substring(1, fileName.length - 1);
  nameB = new Buffer(fileName, "binary");
  fileName = nameB.toString("utf8");
  return fileName;
}

function getOriginFileDataArray(body, boundary) {
  file = querystring.parse(body, "\r\n", ":");
  entireData = body.toString();
  contentType = getContentType(file);
  seperatedArray = entireData.split("Content-Type: " + contentType);
  var finalArr = [];
  for (var i = 0; i < seperatedArray.length; i++) {
    element = seperatedArray[i];
    if (i == 0) {
    } else {
      const shorterData = element.replace(/^\s\s*/, "").replace(/\s\s*$/, "");
      const binaryData = shorterData.substring(
        0,
        shorterData.indexOf("--" + boundary)
      );
      const shortererData = binaryData.substring(0, binaryData.length - 2);
      finalArr.push(shortererData);
    }
  }
  return finalArr;
}

module.exports = upload;
