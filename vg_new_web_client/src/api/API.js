import Network from './Network.js'
export default class API {
  constructor() {
    this.getPrefix = 'api/v1/'
    this.postPrefix = 'managers/'
  }
  apiPostPrefix() {
    return 'api/v1/'
  }
  apiGetPrefix() {
    return 'api/v1/'
  }
  fetch(api, param, method) {
    const fAPI = method == 'GET' ? (this.apiGetPrefix() + api) : (this.apiPostPrefix() + api)
    return Network.request(fAPI, param, method)
  }
}