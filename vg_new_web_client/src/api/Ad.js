import API from './API.js'
import path from 'path'
export default class Ad extends API  {
  postInfo(version, isShow, engURL, cnURL) {
    let fd = {
      version: version,
      show: isShow?1:0,
      link: engURL,
      linkcn: cnURL
    }
    return this.fetch('managers/advertisements', fd, 'POST')
  }
  
  getInfo(platform, language) {
    return this.fetch('managers/advertisements', null, 'GET')
  }
  editLink(link, lang, platform) {
    let fd = {
      'link': link,
      'language': lang,
      'platform': platform
    }
    let api = path.join('managers/ad/link')
    return this.fetch(api, fd, 'POST')
  }

  editShow(isShow) {
    let fd = {
      show: isShow ? '1' : '0'
    }
    let api = path.join('managers/ad/show')
    return this.fetch(api, fd, 'POST')
  }

  editVersion(version) {
    let fd = {
      'version': version
    }
    let api = path.join('managers/ad/version')
    return this.fetch(api, fd, 'POST')
  }
}
