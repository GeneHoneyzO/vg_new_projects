import API from './API.js'
export default class Android extends API {
    postInfo(version, versionCode, engUpdateInfo, cnUpdateInfo) {
        this.version = version
        this.versionCode = versionCode
        this.engUpdateInfo = engUpdateInfo
        this.cnUpdateInfo = cnUpdateInfo
        let param = {
            version: version,
            versionCode: versionCode,
            messageen: engUpdateInfo,
            messagecn: cnUpdateInfo
        }
        return this.fetch('managers/appVersionAndroid', param, 'POST')
    }
    getInfo() {
        return this.fetch('managers/appVersionAndroid', null, 'GET')
    }
}
