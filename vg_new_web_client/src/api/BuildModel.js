import API from './API.js'
export default class BuildModel extends API  {
  postInfo(version, isShow, engURL, cnURL) {
    let fd = {
      version: version,
      show: isShow?1:0,
      link: engURL,
      linkcn: cnURL
    }
    this.fetch('build', fd, 'POST')
    this.version = version
    this.isShow = isShow
    this.engURL = engURL
    this.cnURL = cnURL
  }
  getInfo() {
    return this.fetch('buildmodeltypes', null, 'GET')
  }
}
