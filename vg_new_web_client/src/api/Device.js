import API from './API.js'
export default class Device extends API  {
  getInfo() {
    return this.fetch('managers/firmwareVersions', null, 'GET')
}
  postInfo(mcorePro, mcoreV, megaPro, megaV, orionPro, orionV, starterPro, starterV, aurigaPro, aurigaV) {
    let fd = {
      mcoreprotocol: mcorePro,
      mcoreversion: mcoreV,
      megaPiprotocol: megaPro,
      megaPiversion: megaV,

      orionprotocol: orionPro,
      orionversion: orionV,
      starterprotocol: starterPro,
      starterversion: starterV,

      aurigaprotocol: aurigaPro,
      aurigaversion: aurigaV,
    }
    this.fetch('managers/firmwareVersion', fd, 'POST')
    this.mcorePro = mcorePro
    this.mcoreV = mcoreV
    this.megaPro = megaPro
    this.megaV = megaV

    this.orionPro = orionPro
    this.orionV = orionV
    this.starterPro = starterPro
    this.starterV = starterV

    this.aurigaPro = aurigaPro
    this.aurigaV = aurigaV
  }
}
