import API from './API.js'
export default class Login extends API {

    apiPostPrefix() {
        return ''
    }

    apiGetPrefix() {
        return ''
    }

    login(username, password) {
        this.username = username
        this.password = password
        let fd = {
            username: username,
            password: password
        }
        return this.fetch('login', fd, 'POST')
    }

    logout() {
        return this.fetch('logout', null, 'GET')
    }
}
