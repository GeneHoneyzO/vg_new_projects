var baseURL = require('../../config/index').dev.base
var dev = require('../../config/index').dev
var path = require('path')
const request = function (api, formData, method) {
    // headers.append('Access-Control-Allow-Origin', '*')
    var myInit = {
        'Access-Control-Allow-Origin': '*',
        'Content-Type': 'application/json',
        method: method == 'POST' ? method : 'GET',
        mode: 'cors',
        credentials: 'omit'
    }
    if (formData != null) {
        myInit.body = JSON.stringify(formData)
    } 
    return fetch(api, myInit)
        .then(function (response) {
            if (response.ok) {
                return getContentByType(response)
            } else {
                console.log('hello')
                // throw new Error('error')
                return {
                    code: -99999,
                    message: '网络错误'
                }
            }
        })
}

/** 
 * 根据type返回不同格式的response 
 */
var getContentByType = function (response) {
    var type = response.headers.get('Content-Type').split(";")[0];
    switch (type) {
        case 'text/html':
            return response.text();
            break
        case 'application/json':
            return response.json();
            break
        default:
    }
};

export default {
    request: request
}
