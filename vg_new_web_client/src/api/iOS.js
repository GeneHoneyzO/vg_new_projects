import API from './API.js'
export default class iOS extends API {
    postInfo(version, buildNumber, engUpdateInfo, cnUpdateInfo) {
        this.version = version
        this.buildNumber = buildNumber
        this.engUpdateInfo = engUpdateInfo
        this.cnUpdateInfo = cnUpdateInfo
        let fd = {
            version: version,
            build: buildNumber,
            messageen: engUpdateInfo,
            messagecn: cnUpdateInfo
        }
        return this.fetch('managers/appVersioniOS', fd, 'POST')
    }
    getInfo() {
        return this.fetch('managers/appVersioniOS', null, 'GET')
    }
}
