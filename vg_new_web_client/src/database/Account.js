import dataBase from './datas'

const checkAccountValid = function(param) {
  for (var key in dataBase.users) {
    let user = dataBase.users[key]
    if (user.account == param.account) {
      return true
    }
  }
}

export default {checkAccountValid}