import Vue from "vue";
import App from "./LoginPage";
import ElementUI from "element-ui";
import "element-ui/lib/theme-default/index.css"; // 默认主题
import "babel-polyfill";
Vue.use(ElementUI)
new Vue({
    render: h => h(App)
}).$mount("#login")
