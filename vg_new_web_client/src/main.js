import Vue from "vue";
import App from "./App";
import router from "./router/main.js";
import axios from "axios";
import ElementUI from "element-ui";
import "element-ui/lib/theme-default/index.css"; // 默认主题
import "babel-polyfill";
Vue.use(ElementUI);
Vue.prototype.$axios = axios;
router.beforeEach(function(to, from, next) {
    let login = localStorage.getItem("loginState")
    if (window.location.pathname != '/login' && to.path != "/login" && login != "1") {
        
        next({
            path: '/login'
        })
    } else {
        next();
    }
});

new Vue({
    router,
    render: h => h(App)
}).$mount("#app");
