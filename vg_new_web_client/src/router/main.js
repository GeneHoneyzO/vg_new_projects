import Vue from 'vue';
import Router from 'vue-router';

Vue.use(Router);

export default new Router({
    routes: [
        {
            path: '/',
            redirect: '/login'
        },
        {
            path: '/login',
            component: resolve => require(['../LoginPage.vue'], resolve)
        },
        
        {
            path: '/readme',
            component: resolve => require(['../components/common/Home.vue'], resolve),
            children: [
                {
                    path: '/hometop',
                    component: resolve => require(['../components/page/HomeTopAd.vue'], resolve)
                },
                {
                    path: '/',
                    component: resolve => require(['../components/page/Readme.vue'], resolve)
                }
            ]
        }
    ]
})
